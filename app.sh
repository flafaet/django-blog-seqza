#!/bin/sh

sleep 10

python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py collectstatic  --noinput
gunicorn --bind 0.0.0.0:8000 Blog.wsgi
#python3 manage.py runserver 0.0.0.0:8000

exec "$@"
