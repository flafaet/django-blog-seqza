FROM centos

RUN cd /etc/yum.repos.d/
RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
RUN sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
RUN yum -y update
RUN yum install -y python3 python3-pip libpq-devel git
RUN cd opt && git clone https://gitlab.com/flafaet/django-blog-seqza.git
WORKDIR /opt/django-blog-seqza
RUN pip3 install -r requirements.txt
RUN chmod +x app.sh
